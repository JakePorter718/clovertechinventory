﻿using Microsoft.Practices.Prism.Commands;
using System.Windows.Input;
using CloverTechInventory.DataAccess;
using CloverTechInventory.Model;
using System.Collections.Generic;
using System.Windows;
using System.Linq;

namespace CloverTechInventory.ViewModel
{
    public class CreateViewModel : ViewModelBase
    {
        #region Instance

        BuildRepo buildRepo = new BuildRepo();
        PartRepo partRepo = new PartRepo();

        #endregion

        #region Property

        private string _buildName;      
        public string buildName
        {
            get { return _buildName; }
            set
            {
                if (_buildName != value)
                {
                    _buildName = value;
                    OnPropertyChanged("buildName");
                }
            }
        }

        private int _partCount;
        public int PartCount
        {
            get { return _partCount; }
            set
            {
                if (_partCount != value)
                {
                    _partCount = value;
                    OnPropertyChanged("PartCount");
                }
            }
        }

        private List<Part> _parts;
        public List<Part> Parts
        {
            get { return _parts; }
            set
            {
                if (_parts != value)
                {
                    _parts = value;
                    OnPropertyChanged("Parts");
                }
            }
        }

        private Part _selectedPart;
        public Part SelectedPart
        {
            get { return _selectedPart; }
            set
            {
                if (_selectedPart != value)
                {
                    _selectedPart = value;
                    OnPropertyChanged("SelectedPart");
                    if (SelectedPart != null)
                    {
                        PartName = SelectedPart.partName;
                        PartValue = SelectedPart.partValue;
                        PartPrice = SelectedPart.partPrice;
                        PartManufacture = SelectedPart.manufacture;
                        partManufactureNumber = SelectedPart.manufactureNumber;
                    }
                    else
                    {
                        PartName = default(string);
                        PartValue = default(string);
                        PartPrice = default(string);
                        PartManufacture = default(string);
                        partManufactureNumber = default(string);
                    }
                    
                }
            }
        }

        private List<Part> _buildParts;
        public List<Part> BuildParts
        {
            get { return _buildParts; }
            set
            {
                if (_buildParts != value)
                {
                    _buildParts = value;
                    OnPropertyChanged("BuildParts");
                }
            }
        }

        private string _selectedBuildNames;
        public string SelectedBuildNames
        {
            get { return _selectedBuildNames; }
            set
            {
                if (_selectedBuildNames != value)
                {
                    _selectedBuildNames = value;
                    OnPropertyChanged("SelectedBuildNames");
                    if (SelectedBuildNames != null )
                    {
                        SelectedBuilds = Builds.FirstOrDefault(b => b.buildName == SelectedBuildNames);
                    }
                }
            }
        }

        private List<string> _buildNames;
        public List<string> BuildNames
        {
            get { return _buildNames; }
            set
            {
                if (_buildNames != value)
                {
                    _buildNames = value;
                    OnPropertyChanged("BuildNames");
                }
            }
        }

        private List<Build> _builds;
        public List<Build> Builds
        {
            get { return _builds; }
            set
            {
                if (_builds != value)
                {
                    _builds = value;
                    OnPropertyChanged("Builds");
                    foreach (var item in Builds)
                    {
                        if (BuildNames.FirstOrDefault(b => b.Contains(item.buildName)) == null)
                        {
                            BuildNames.Add(item.buildName);
                        }
                    }
                }
            }
        }

        private Build _selectedBuilds;
        public Build SelectedBuilds
        {
            get { return _selectedBuilds; }
            set
            {
                if (_selectedBuilds != value)
                {
                    _selectedBuilds = value;
                    OnPropertyChanged("SelectedBuilds");
                    if (SelectedBuilds != null)
                    {
                        
                        List<Build> bs = Builds.Where(b => b.buildName == SelectedBuilds.buildName).ToList();
                        BuildParts.Clear();
                        Part prt = new Part();
                        prt.partName = "Name";
                        prt.partValue = "Value";
                        prt.partPrice = "Price";
                        prt.manufacture = "Manufacture";
                        prt.manufactureNumber = "Manufacture #";
                        BuildParts.Add(prt);
                        foreach (var item in bs)
                        {
                            BuildParts.Add(Parts.FirstOrDefault(p => p.part_id == item.part_id));
                        }
                        BuildParts = new List<Part>(BuildParts);
                    }
                    else
                        BuildParts.Clear();
                }
            }
        }

        private string _partName;
        public string PartName
        {
            get { return _partName; }
            set
            {
                if (_partName != value)
                {
                    _partName = value;
                    OnPropertyChanged("PartName");
                }
            }
        }

        private string _partValue;
        public string PartValue
        {
            get { return _partValue; }
            set
            {
                if (_partValue != value)
                {
                    _partValue = value;
                    OnPropertyChanged("PartValue");
                }
            }
        }

        private string _partPrice;
        public string PartPrice
        {
            get { return _partPrice; }
            set
            {
                if (_partPrice != value)
                {
                    _partPrice = value;
                    OnPropertyChanged("PartPrice");
                }
            }
        }

        private string _partManufacture;
        public string PartManufacture
        {
            get { return _partManufacture; }
            set
            {
                if (_partManufacture != value)
                {
                    _partManufacture = value;
                    OnPropertyChanged("PartManufacture");
                }
            }
        }

        private string _partManufactureNumber;
        public string partManufactureNumber
        {
            get { return _partManufactureNumber; }
            set
            {
                if (_partManufactureNumber != value)
                {
                    _partManufactureNumber = value;
                    OnPropertyChanged("partManufactureNumber");
                }
            }
        }

        #endregion

        #region Commands

        public ICommand updatePartCommand { get; set; }
        public ICommand addPartToBuildCommand { get; set; }
        public ICommand insertNewPartCommand { get; set; }
        public ICommand deletePartCommand { get; set; }
        public ICommand newBuildCommand { get; set; }

        #endregion

        #region Constructor

        public CreateViewModel()
        {
            updatePartCommand = new DelegateCommand(UpdatePartCommand);
            addPartToBuildCommand = new DelegateCommand(AddPartToBuildCommand);
            insertNewPartCommand = new DelegateCommand(InsertNewPartCommand);
            deletePartCommand = new DelegateCommand(DeletePartCommand);
            newBuildCommand = new DelegateCommand(NewBuildCommand);
            BuildNames = new List<string>();
            BuildParts = new List<Part>();
            Builds = buildRepo.GetAll();
            Parts = partRepo.GetAll();
        }

        #endregion

        #region Command Handlers

        void AddPartToBuildCommand()
        {
            buildRepo.Insert(SelectedBuilds.buildName, SelectedPart.part_id, PartCount);
            Builds = new List<Build>(buildRepo.GetAll());
        }

        void DeletePartCommand()
        {
            partRepo.Delete(SelectedPart.part_id);
            Parts = new List<Part>(partRepo.GetAll());
        }

        void InsertNewPartCommand()
        {
            partRepo.Insert(PartName, PartValue, PartPrice, PartManufacture, partManufactureNumber);
            Parts = new List<Part>(partRepo.GetAll());
        }

        void UpdatePartCommand()
        {
            partRepo.Update(SelectedPart.part_id, SelectedPart.partName, PartValue, PartPrice, PartManufacture, partManufactureNumber, SelectedPart.partCount, SelectedPart.partMinAmount);
            Parts = new List<Part>(partRepo.GetAll());
        }
    

        void NewBuildCommand()
        {
            if (buildName != null && SelectedPart != null)
            {
                buildRepo.Insert(buildName, SelectedPart.part_id, PartCount);
                Builds = new List<Build>(buildRepo.GetAll());
            }
            else
                MessageBox.Show("Please type in a name and also selected a beginning part.","Error");
        }

        #endregion
    }
}
