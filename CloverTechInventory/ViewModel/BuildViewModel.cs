﻿using CloverTechInventory.DataAccess;
using CloverTechInventory.Model;
using Microsoft.Practices.Prism.Commands;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Input;

namespace CloverTechInventory.ViewModel
{
    public class BuildViewModel : ViewModelBase
    {
        #region Instance

        BuildRepo buildrepo = new BuildRepo();

        #endregion

        #region Property

        private List<string> _buildNames;
        public List<string> BuildNames
        {
            get { return _buildNames; }
            set
            {
                if (_buildNames != value)
                {
                    _buildNames = value;
                    OnPropertyChanged("BuildNames");
                }
            }
        }

        private string _selectedBuildName;
        public string SelectedBuildName
        {
            get { return _selectedBuildName; }
            set
            {
                if (_selectedBuildName != value)
                {
                    _selectedBuildName = value;
                    OnPropertyChanged("SelectedBuildName");
                }
            }
        }

        private List<Build> _builds;
        public List<Build> Builds
        {
            get { return _builds; }
            set
            {
                if (_builds != value)
                {
                    _builds = value;
                    OnPropertyChanged("Builds");
                    if (Builds != null)
                    {
                        foreach (var item in Builds)
                        {
                            if (BuildNames.FirstOrDefault(bn => bn.Contains(item.buildName)) == null)
                            {
                                BuildNames.Add(item.buildName);
                            }
                        }
                    }
                }
            }
        }

        private string _amountNeeded;
        public string AmountNeeded
        {
            get { return _amountNeeded; }
            set
            {
                if (_amountNeeded != value)
                {
                    _amountNeeded = value;
                    OnPropertyChanged("AmountNeeded");
                }
            }
        }

        private string _amountOnHand;
        public string AmountOnHand
        {
            get { return _amountOnHand; }
            set
            {
                if (_amountOnHand != value)
                {
                    _amountOnHand = value;
                    OnPropertyChanged("AmountOnHand");
                }
            }
        }

        #endregion

        #region Commands

        public ICommand exportBuildCommand { get; set; }

        #endregion

        #region Constructor

        public BuildViewModel()
        {
            exportBuildCommand = new DelegateCommand(ExportBuildCommand);
            BuildNames = new List<string>();
            Builds = buildrepo.GetAll();
        }

        #endregion

        #region Command Handlers

        public void ExportBuildCommand()
        {

        }

        #endregion
    }
}
