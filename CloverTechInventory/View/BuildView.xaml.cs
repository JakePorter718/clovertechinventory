﻿using System.Windows.Controls;
using CloverTechInventory.ViewModel;

namespace CloverTechInventory.View
{
    /// <summary>
    /// Interaction logic for BuildView.xaml
    /// </summary>
    public partial class BuildView : UserControl
    {
        public BuildView()
        {
            InitializeComponent();
            BuildViewModel bv = new BuildViewModel();
            DataContext = bv;
        }
    }
}
