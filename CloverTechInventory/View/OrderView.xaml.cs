﻿using System.Windows.Controls;
using CloverTechInventory.ViewModel;

namespace CloverTechInventory.View
{
    /// <summary>
    /// Interaction logic for OrderView.xaml
    /// </summary>
    public partial class OrderView : UserControl
    {
        public OrderView()
        {
            InitializeComponent();
            OrderViewModel ov = new OrderViewModel();
            DataContext = ov;
        }
    }
}
