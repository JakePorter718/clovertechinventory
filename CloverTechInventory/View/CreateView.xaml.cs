﻿using System.Windows.Controls;
using CloverTechInventory.ViewModel;

namespace CloverTechInventory.View
{
    /// <summary>
    /// Interaction logic for CreateView.xaml
    /// </summary>
    public partial class CreateView : UserControl
    {
        public CreateView()
        {
            InitializeComponent();
            CreateViewModel cvm = new CreateViewModel();
            DataContext = cvm;
        }
    }
}
