﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloverTechInventory.Model
{
    public class Build
    {
        public int build_id { get; set; }
        public string buildName { get; set; }
        public int part_id { get; set; }
        public int partCount { get; set; }
    }
}
