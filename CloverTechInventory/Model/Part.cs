﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloverTechInventory.Model
{
    public class Part
    {
        public int part_id { get; set; }
        public string partName { get; set; }
        public string partValue { get; set; }
        public string partPrice { get; set; }
        public string manufacture { get; set; }
        public string manufactureNumber { get; set; }
        public int partCount { get; set; }
        public int partMinAmount { get; set; }
    }
}
