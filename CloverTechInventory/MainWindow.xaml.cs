﻿using CloverTechInventory.ViewModel;
using System.Windows;

namespace CloverTechInventory
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            MainViewModel main = new MainViewModel();
            DataContext = main;
        }
    }
}
