﻿using CloverTechInventory.Model;
using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloverTechInventory.DataAccess
{
    public class PartRepo
    {
        string dbConnectionName = @"URI=file:C:\Users\Jacob\SkyDrive\Documents\Visual Studios\CloverTechInventory\packages\Inventory.db";

        public void Insert(string partname, string partvalue, string partprice, string manufacture, string manufacturenumber)
        {
            using (SQLiteConnection conn = new SQLiteConnection(dbConnectionName))
            {
                conn.Open();
                string sqlStatement = "INSERT INTO Part " +
                                      "(partName, partValue, partPrice, manufacture, manufactureNumber, partCount, partMinAmount) " +
                                      "VALUES('" + partname + "','" + partvalue + "','"+ partprice +"','"+ manufacture +"','"+ manufacturenumber + "'," + 0 + "," + 0 +"); ";
                using (SQLiteCommand cmd = new SQLiteCommand(sqlStatement, conn))
                {
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public void Update(int part_id, string partname, string partvalue, string partprice, string manufacture, string manufacturenumber, int partcount, int partminamount)
        {
            using (SQLiteConnection conn = new SQLiteConnection(dbConnectionName))
            {
                conn.Open();
                string sqlStatement = "UPDATE Part " +
                                      "SET partName = '" + partname +
                                      "', partValue = '" + partvalue +
                                      "', partPrice = '" + partprice +
                                      "', manufacture = '" + manufacture +
                                      "', manufactureNumber = '" + manufacturenumber +
                                      "', partCount = '" + partcount +
                                      "', partMinAmount = '" + partminamount +
                                      "' Where part_id = " + part_id + ";";
                using (SQLiteCommand cmd = new SQLiteCommand(sqlStatement, conn))
                {
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public List<Part> GetAll()
        {
            using (SQLiteConnection conn = new SQLiteConnection(dbConnectionName))
            {
                List<Part> parts = new List<Part>();
                conn.Open();
                string sqlStatement = "SELECT part_id, partName, partValue, partPrice, manufacture, manufactureNumber, partCount, partMinAmount " +
                                      "FROM Part";
                using (SQLiteCommand cmd = new SQLiteCommand(sqlStatement, conn))
                {
                    using (SQLiteDataReader rdr = cmd.ExecuteReader())
                    {
                        while (rdr.Read())
                        {
                            Part result = new Part();
                            result.part_id = rdr.GetInt32(0);
                            result.partName = rdr.GetString(1);
                            result.partValue = rdr.GetString(2);
                            result.partPrice = rdr.GetString(3);
                            result.manufacture = rdr.GetString(4);
                            result.manufactureNumber = rdr.GetString(5);
                            result.partCount = rdr.GetInt32(6);
                            result.partMinAmount = rdr.GetInt32(7);
                            parts.Add(result);
                        }
                    }
                }
                return parts;
            }
        }

        public void Delete(int part_id)
        {
            using (SQLiteConnection conn = new SQLiteConnection(dbConnectionName))
            {
                conn.Open();
                string sqlStatement = "DELETE " +
                                      "FROM Part " +
                                      "WHERE part_id = " + part_id;
                using (SQLiteCommand cmd = new SQLiteCommand(sqlStatement, conn))
                {
                    cmd.ExecuteNonQuery();
                }
            }
        }
    }
}
