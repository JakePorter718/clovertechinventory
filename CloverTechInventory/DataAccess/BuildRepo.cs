﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;
using CloverTechInventory.Model;

namespace CloverTechInventory.DataAccess
{
    public class BuildRepo
    {
        string dbConnectionName = @"URI=file:C:\Users\Jacob\SkyDrive\Documents\Visual Studios\CloverTechInventory\packages\Inventory.db";

        public void Insert(string build, int part_id, int partCount)
        {
            using (SQLiteConnection conn = new SQLiteConnection(dbConnectionName))
            {
                conn.Open();
                string sqlStatement = "INSERT INTO Build "+
                               "(buildName, part_id, partCount) "+
                               "VALUES('"+ build +"',"+ part_id +","+ partCount +");";
                using (SQLiteCommand cmd = new SQLiteCommand(sqlStatement, conn))
                {
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public List<Build> GetAll()
        {
            using (SQLiteConnection conn = new SQLiteConnection(dbConnectionName))
            {
                List<Build> builds = new List<Build>();
                conn.Open();
                string sqlStatement = "SELECT build_id, buildName, part_id, partCount "+
                                      "FROM Build";
                using (SQLiteCommand cmd = new SQLiteCommand(sqlStatement, conn))
                {
                    using (SQLiteDataReader rdr = cmd.ExecuteReader())
                    {
                        while (rdr.Read())
                        {
                            Build result = new Build();
                            result.build_id = rdr.GetInt32(0);
                            result.buildName = rdr.GetString(1);
                            result.part_id = rdr.GetInt32(2);
                            result.partCount = rdr.GetInt32(3);
                            builds.Add(result);
                        }
                    }
                }
                return builds;
            }
        }

        public void Delete(int build_id)
        {
            using (SQLiteConnection conn = new SQLiteConnection(dbConnectionName))
            {
                conn.Open();
                string sqlStatement = "DELETE " +
                                      "FROM Build " +
                                      "WHERE build_id = " + build_id;
                using (SQLiteCommand cmd = new SQLiteCommand(sqlStatement, conn))
                {
                    cmd.ExecuteNonQuery();
                }
            }
        }
    }
}
